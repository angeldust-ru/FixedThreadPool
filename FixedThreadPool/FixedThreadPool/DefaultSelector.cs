﻿using System;

namespace FixedThreadPool
{
	public class DefaultSelector : ITaskSelector
	{
		private readonly IPriorityQueue _queue;

		public DefaultSelector(IPriorityQueue queue)
		{
			if (queue == null) throw new ArgumentNullException(nameof(queue), "Queue can't be null");
			_queue = queue;
		}

		public ITask Next()
		{
			if (_higt < 3)
			{
				if (_queue.CountByPriority(Priority.HIGH) > 0)
				{
					_higt++;
					return _queue.Dequeue(Priority.HIGH);
				}
			}
			else _higt = 0;
			
			if (_queue.CountByPriority(Priority.NORMAL) > 0)
			{
				_normal++;
				return _queue.Dequeue(Priority.NORMAL);
			}
			
			if (_queue.CountByPriority(Priority.LOW) > 0)
					return _queue.Dequeue(Priority.LOW);

			return null;
		}
		



		private Priority _lastLvl;
		private int _low = 0;
		private int _higt = 0;
		private int _normal = 0;
	}
}