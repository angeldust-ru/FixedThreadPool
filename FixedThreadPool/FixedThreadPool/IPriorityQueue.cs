﻿namespace FixedThreadPool
{
	public interface IPriorityQueue
	{
		int Count { get; }

		int CountByPriority(Priority priority);
		ITask Dequeue(Priority priority);
		bool Enqueue(ITask task, Priority priority);
	}
}