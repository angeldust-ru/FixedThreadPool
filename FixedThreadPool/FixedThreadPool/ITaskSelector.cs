﻿namespace FixedThreadPool
{
	public interface ITaskSelector
	{
		ITask Next();
	}
}