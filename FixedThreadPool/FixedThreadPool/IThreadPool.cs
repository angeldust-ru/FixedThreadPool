﻿using System.Net.Mail;

namespace FixedThreadPool
{
	public interface IThreadPool
	{
		bool Execute(ITask task, Priority priority);
		void Stop();
	}
}