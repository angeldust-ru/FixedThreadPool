﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace FixedThreadPool
{
	public sealed class PriorityQueue : IPriorityQueue
	{
		private readonly ConcurrentDictionary<Priority, ConcurrentQueue<ITask>> _queues;

		/// <summary>
		/// Total of tasks in queue.
		/// </summary>
		public int Count
		{
			get { return _queues.Values.Sum(tasks => tasks.Count); }
		}

		public PriorityQueue()
		{
			_queues = new ConcurrentDictionary<Priority, ConcurrentQueue<ITask>>();
		}

		public bool Enqueue(ITask task, Priority priority)
		{
			if (task == null) throw new ArgumentNullException(nameof(task), "Argument can't b null");

			ConcurrentQueue<ITask> queue = null;
			if (!_queues.TryGetValue(priority, out queue))
			{
				queue = new ConcurrentQueue<ITask>();
				queue.Enqueue(task);
				return _queues.TryAdd(priority, queue);
				
			}
			queue.Enqueue(task);
			return true;
		}


		public int CountByPriority(Priority priority)
		{
			ConcurrentQueue<ITask> queue;
			if (_queues.TryGetValue(priority, out queue) && queue!=null)
			{
				return queue.Count;
			}
			return 0;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="priority"></param>
		/// <exception cref="InvalidOperationException">If queue not found or empty.</exception>
		/// <returns></returns>
		public ITask Dequeue(Priority priority)
		{
			ConcurrentQueue<ITask> queue = null;
			if (_queues.TryGetValue(priority, out queue))
			{
				ITask tmp;
				if (queue.TryDequeue(out tmp))
					return tmp;
			}
			throw new InvalidOperationException($"Queue by priority {priority} not exist.");
		}
	}
}