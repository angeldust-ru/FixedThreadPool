﻿using System;
using System.Threading;

namespace FixedThreadPool
{
	public sealed class ThreadPool : IThreadPool
	{
		private IPriorityQueue _queue;
		private ITaskSelector _selector;
		/// <summary>
		/// This is fixed thread pool.
		/// </summary>
		/// <param name="tc"> Count of threads to work.</param>
		/// <exception cref="ArgumentOutOfRangeException"> If <param name="tc"></param> less one.</exception>
		public ThreadPool(int tc)
		{
			if (tc < 1) throw new ArgumentOutOfRangeException(nameof(tc), "Thread count can\'t be less one.");
			_threads = new Thread[tc];
		}

		/// <summary>
		/// Set task in to queue by <see cref="Priority"/>.
		/// </summary>
		/// <param name="task"></param>
		/// <param name="priority"></param>
		/// <returns>True if task success added else false.</returns>
		public bool Execute(ITask task, Priority priority)
		{
			if(task==null)throw new ArgumentNullException(nameof(task),"Task to run can't be null.");
			_queue.Enqueue(task,priority);
			return true;
		}

		private void Work()
		{
			while (true)
			{
				ITask t=_selector.Next();
				t?.Execute();
			}
		}

		public void Stop()
		{
			throw new NotImplementedException();
		}

		private Thread[] _threads;
	}
}