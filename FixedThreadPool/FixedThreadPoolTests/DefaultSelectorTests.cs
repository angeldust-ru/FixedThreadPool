﻿using NUnit.Framework;
using FixedThreadPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixedThreadPool.Tests
{
	[TestFixture()]
	public class DefaultSelectorTests
	{
		[Test()]
		[Ignore("No implement")]
		public void DefaultSelectorTest()
		{
			Assert.Fail();
		}

		[Test()]
		public void Next_3High1Normal_InfinityQueue_Test()
		{
			IPriorityQueue queue = new InfinityAllQueue();
			Assert.AreEqual(1, ((Task) queue.Dequeue(Priority.HIGH)).Id);
			Assert.AreEqual(2, ((Task) queue.Dequeue(Priority.NORMAL)).Id);
			Assert.AreEqual(3, ((Task) queue.Dequeue(Priority.LOW)).Id);

			ITaskSelector selector = new DefaultSelector(queue);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(2, ((Task) selector.Next()).Id);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(2, ((Task) selector.Next()).Id);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(1, ((Task) selector.Next()).Id);
			Assert.AreEqual(2, ((Task) selector.Next()).Id);
		}

		[Test()]
		public void Next_4High10Normal1Low_4High10Normal10LowQueue_Test()
		{
			IPriorityQueue queue=new FourHighTenNormalTenLowQueue();
			Assert.AreEqual(4,queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(10,queue.CountByPriority(Priority.NORMAL));
			Assert.AreEqual(10,queue.CountByPriority(Priority.LOW));

			ITaskSelector selector=new DefaultSelector(queue);
			Assert.AreEqual(1, ((Task)selector.Next()).Id);
			Assert.AreEqual(1, ((Task)selector.Next()).Id);
			Assert.AreEqual(1, ((Task)selector.Next()).Id);

			Assert.AreEqual(2, ((Task)selector.Next()).Id);

			Assert.AreEqual(1, ((Task)selector.Next()).Id);

			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);

			Assert.AreEqual(3, ((Task)selector.Next()).Id);
		}

		[Test()]
		public void Next_4High3Normal1Low_DinamicQueue_Test()
		{
			IPriorityQueue queue=new DinamicQueue(3,3,3);
			Assert.AreEqual(3,queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(3,queue.CountByPriority(Priority.NORMAL));
			Assert.AreEqual(3,queue.CountByPriority(Priority.LOW));

			ITaskSelector selector=new DefaultSelector(queue);
			Assert.AreEqual(1, ((Task)selector.Next()).Id);
			Assert.AreEqual(1, ((Task)selector.Next()).Id);
			Assert.AreEqual(1, ((Task)selector.Next()).Id);

			Assert.AreEqual(0,queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(3,queue.CountByPriority(Priority.NORMAL));
			Assert.AreEqual(3,queue.CountByPriority(Priority.LOW));

			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(2, ((Task)selector.Next()).Id);
			Assert.AreEqual(0,queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(1,queue.CountByPriority(Priority.NORMAL));
			Assert.AreEqual(3,queue.CountByPriority(Priority.LOW));

			queue.Enqueue(null,Priority.HIGH);
			Assert.AreEqual(1, queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(1, queue.CountByPriority(Priority.NORMAL));
			Assert.AreEqual(3, queue.CountByPriority(Priority.LOW));

			Assert.AreEqual(1, ((Task)selector.Next()).Id);

			Assert.AreEqual(2, ((Task)selector.Next()).Id);

			Assert.AreEqual(3, ((Task)selector.Next()).Id);
		}
	}

	internal class InfinityAllQueue : IPriorityQueue
	{
		public int Count
		{
			get { return 30; }
		}

		public int CountByPriority(Priority priority)
		{
			return 10;
		}

		public ITask Dequeue(Priority priority)
		{
			switch (priority)
			{
				case Priority.HIGH:
					return new Task() {Id = 1};
				case Priority.NORMAL:
					return new Task() {Id = 2};
				case Priority.LOW:
					return new Task() {Id = 3};
			}
			return null;
		}

		public bool Enqueue(ITask task, Priority priority)
		{
			throw new NotImplementedException();
		}
	}

	internal class FourHighTenNormalTenLowQueue : IPriorityQueue
	{
		private int _high = 4;
		private int _normal = 10;
		private int _low = 10;

		public int Count
		{
			get { return _high + _normal + _low; }
		}

		public int CountByPriority(Priority priority)
		{
			switch (priority)
			{
				case Priority.HIGH:
					return _high;
				case Priority.NORMAL:
					return _normal;
				case Priority.LOW:
					return _low;
			}
			throw new ArgumentOutOfRangeException();
		}

		public ITask Dequeue(Priority priority)
		{
			switch (priority)
			{
				case Priority.HIGH:
					if (_high == 0) return null;
					_high--;
					return new Task() {Id = 1};
				case Priority.NORMAL:
					if (_normal == 0) return null;
					_normal--;
					return new Task() {Id = 2};
				case Priority.LOW:
					if (_low == 0) return null;
					_low--;
					return new Task() {Id = 3};
			}
			return null;
		}

		public bool Enqueue(ITask task, Priority priority)
		{
			throw new NotImplementedException();
		}
	}
	internal class DinamicQueue : IPriorityQueue
	{
		private int _high = 0;
		private int _normal = 0;
		private int _low = 0;

		public int Count
		{
			get { return _high + _normal + _low; }
		}

		public DinamicQueue(int high =0, int normal=0, int low=0)
		{
			_high = high;
			_normal = normal;
			_low = low;
		}

		public int CountByPriority(Priority priority)
		{
			switch (priority)
			{
				case Priority.HIGH:
					return _high;
				case Priority.NORMAL:
					return _normal;
				case Priority.LOW:
					return _low;
			}
			throw new ArgumentOutOfRangeException();
		}

		public ITask Dequeue(Priority priority)
		{
			switch (priority)
			{
				case Priority.HIGH:
					if (_high == 0) return null;
					_high--;
					return new Task() {Id = 1};
				case Priority.NORMAL:
					if (_normal == 0) return null;
					_normal--;
					return new Task() {Id = 2};
				case Priority.LOW:
					if (_low == 0) return null;
					_low--;
					return new Task() {Id = 3};
			}
			return null;
		}

		public bool Enqueue(ITask task, Priority priority)
		{
			switch (priority)
			{
				case Priority.HIGH:
					_high++;
					return true;
				case Priority.NORMAL:
					_normal++;
					return true;
				case Priority.LOW:
					_low++;
					return true;
			}
			return false;
		}
	}
}