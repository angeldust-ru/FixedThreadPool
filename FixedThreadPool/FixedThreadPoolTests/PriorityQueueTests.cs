﻿using NUnit.Framework;
using FixedThreadPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixedThreadPool.Tests
{
	[TestFixture()]
	public class PriorityQueueTests
	{

		[Test()]
		public void EnqueueTest()
		{
			PriorityQueue queue=new PriorityQueue();
			queue.Enqueue(new Task() {Id = 1}, Priority.HIGH);
			queue.Enqueue(new Task() {Id = 2}, Priority.LOW);
			queue.Enqueue(new Task() {Id = 3}, Priority.NORMAL);
			queue.Enqueue(new Task() {Id = 4}, Priority.HIGH);
			queue.Enqueue(new Task() {Id = 5}, Priority.LOW);
			queue.Enqueue(new Task() {Id = 6}, Priority.NORMAL);
			Assert.AreEqual(6,queue.Count);
			Assert.AreEqual(2, queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(2, queue.CountByPriority(Priority.LOW));
			Assert.AreEqual(2, queue.CountByPriority(Priority.NORMAL));
		}
		[Test()]
		public void Enqueue_HighOnly_Test()
		{
			PriorityQueue queue=new PriorityQueue();
			Assert.IsTrue(queue.Enqueue(new Task() {Id = 1}, Priority.HIGH));
			Assert.IsTrue(queue.Enqueue(new Task() {Id = 3}, Priority.HIGH));
			Assert.IsTrue(queue.Enqueue(new Task() {Id = 5}, Priority.HIGH));
			Assert.AreEqual(3,queue.Count);
			Assert.AreEqual(3, queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(0, queue.CountByPriority(Priority.LOW));
			Assert.AreEqual(0, queue.CountByPriority(Priority.NORMAL));
		}

		[Test()]
		public void Enqueue_LowOnly_Test()
		{
			PriorityQueue queue=new PriorityQueue();
			queue.Enqueue(new Task() {Id = 1}, Priority.LOW);
			queue.Enqueue(new Task() {Id = 3}, Priority.LOW);
			queue.Enqueue(new Task() {Id = 5}, Priority.LOW);
			Assert.AreEqual(3,queue.Count);
			Assert.AreEqual(0, queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(3, queue.CountByPriority(Priority.LOW));
			Assert.AreEqual(0, queue.CountByPriority(Priority.NORMAL));
		}

		[Test()]
		public void Enqueue_NormalOnly_Test()
		{
			PriorityQueue queue=new PriorityQueue();
			queue.Enqueue(new Task() {Id = 1}, Priority.NORMAL);
			queue.Enqueue(new Task() {Id = 3}, Priority.NORMAL);
			queue.Enqueue(new Task() {Id = 5}, Priority.NORMAL);
			Assert.AreEqual(3,queue.Count);
			Assert.AreEqual(0, queue.CountByPriority(Priority.HIGH));
			Assert.AreEqual(0, queue.CountByPriority(Priority.LOW));
			Assert.AreEqual(3, queue.CountByPriority(Priority.NORMAL));
		}


		[Test()]
		public void Dequeue_High_Test()
		{
			PriorityQueue queue = new PriorityQueue();
			queue.Enqueue(new Task() { Id = 1 }, Priority.HIGH);
			queue.Enqueue(new Task() { Id = 2 }, Priority.HIGH);
			queue.Enqueue(new Task() { Id = 3 }, Priority.HIGH);
			queue.Enqueue(new Task() { Id = 4 }, Priority.HIGH);
			Assert.AreEqual(4, queue.Count);
			Assert.AreEqual(1,((Task)queue.Dequeue(Priority.HIGH)).Id);
			Assert.AreEqual(3,queue.Count);
			Assert.AreEqual(2,((Task)queue.Dequeue(Priority.HIGH)).Id);
			Assert.AreEqual(2, queue.Count);
			Assert.AreEqual(3,((Task)queue.Dequeue(Priority.HIGH)).Id);
			Assert.AreEqual(1, queue.Count);
			Assert.AreEqual(4,((Task)queue.Dequeue(Priority.HIGH)).Id);
			Assert.AreEqual(0, queue.Count);
		}
	}

	internal class Task : ITask
	{
		internal int Id { get; set; }
		public void Execute()
		{
		}
	}
}